# face_check

## Project setup

npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

```

 实现样式为 :blush: 

+ 移动端首页提示

![移动端首页提示](https://images.gitee.com/uploads/images/2020/0807/164412_c7cd85a4_7648944.png "1.png")

+ 生成朗读随机数

![朗读随机数](https://images.gitee.com/uploads/images/2020/0807/164457_32a8c8d9_7648944.png "2.png")

+ 频繁操作错误提示

![频繁操作错误提示](https://images.gitee.com/uploads/images/2020/0807/164538_a249c15a_7648944.png "3.png")
